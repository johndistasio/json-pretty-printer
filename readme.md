# Readme

This is a very simple command line JSON pretty printer.

	> python jpp.py <input file path>

or

	> python jpp.py <input file path> <output file path>


If an output file path is not given, the script writes to ./output.json. It will blindly overwrite whatever file it is outputting to.
