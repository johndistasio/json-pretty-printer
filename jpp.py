import json
import sys


class json_pretty_print:
	"""Quick and dirty class to handle the pretty printing of JSON"""

	#The file we want to pretty print
	input_file = None

	#The file we want to pretty print to
	output_file = None

	def __init__(self, input_file_name, output_file_name):
		"""Opens the files we want to work with."""
		
		#Open the file to read from
		if input_file_name:
			try:
				self.input_file = open(input_file_name)
			except IOError:
				print 'Could not open input file "' + input_file_name + '", script will terminate.'
				sys.exit()

		#If we weren't given a specific output file to write to, just write to the default
		if output_file_name is None:
			self.output_file = 'output.json'

		#Open our target file		
		try:
			self.output_file = open(self.output_file, 'w')
		except IOError:
			print 'Could not open output file "' + output_file + '", script will terminate.'
			sys.ext()
		
	def pretty_print(self):
		
		try:
			js = json.load(self.input_file)
		except ValueError:
			print 'Couldn\'t parse JSON from file "' + self.input_file.name + '", script will terminate.'
			sys.exit()

		json.dump(js, self.output_file, sort_keys=False, indent=2, ensure_ascii=True)




if __name__ == '__main__':

	if len(sys.argv) == 3:
		jpp = json_pretty_print(sys.argv[1], sys.argv[2])
	elif len(sys.argv) == 2:
		jpp = json_pretty_print(sys.argv[1], None)
	else:
		print 'No input file provided.'

	jpp.pretty_print()
